using UnityEngine;
using System.Collections;

public class PickupScore : MonoBehaviour {

	public int score;
	public float inactiveDuration;
	
	void OnTriggerEnter(Collider other) {
		FloatingObject fo = other.gameObject.GetComponent<FloatingObject>();
		
		if(fo == null) return;
		
		StartCoroutine(Deactivate(inactiveDuration));
		
		Debug.Log("Score " + score + " for " + fo.name);
	}
	
	IEnumerator Deactivate(float duration) {
		renderer.enabled = false;
		collider.enabled = false;
		yield return new WaitForSeconds(duration);
		renderer.enabled = true;
		collider.enabled = true;
	}
}
