using UnityEngine;
using System.Collections;

public class TimerWaypoint : MonoBehaviour {
	
	public string name;
	public bool isStartingLine;
	public bool isFinishLine;
	
	void OnTriggerEnter(Collider other) {
		
		//Debug.Log("Hit!");
		
		//What object hit us?
		FloatingObject fo = other.gameObject.GetComponent<FloatingObject>();
		
		if(fo == null) return;
		
		//First, check if is start or ending line:
		if(isStartingLine) {
			fo.StartClock();
		}
		
		if(isFinishLine) {
			fo.StopClock();
		}
		
		fo.AddWaypointEvent(fo.GetCurrentTime(), this.name);
		
		GUIcoordinator.Instance.UpdateStats();
	}

}
