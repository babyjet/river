using UnityEngine;
using System.Collections;

public class PlayerOnPath : MonoBehaviour {
	
	public BezierPath path;
	public float speed;
	float currentPathDist;
	float targetDist;
	float rotAngle;
	Vector3 rotTargetPos;
	
	private Animator anim;
	
	
	void Start() {
		
		anim = GetComponent<Animator>();
		
		foreach(PathPoint p in FindObjectsOfType(typeof(PathPoint))) {
			p.pathPointClickEvent += OnPathPointEvent;
		}
	}
	
	void OnPathPointEvent(PathPoint p) {
		targetDist = p.pathDist;
		
	}
	
	void Update() {
		
	// create angle from current position and target position 0.1 further away on the path
		rotTargetPos = path.GetPositionByDistance (currentPathDist + 0.1f * Mathf.Sign (targetDist - currentPathDist));
		rotAngle = Mathf.Atan2 (rotTargetPos.x - transform.position.x, 
								rotTargetPos.z - transform.position.z) 
								* Mathf.Rad2Deg;
		transform.localRotation = Quaternion.Euler(new Vector3(0, rotAngle, 0));
		
	// stop animation speed if character stops on path
		if(Mathf.Abs(targetDist - currentPathDist) < .05f) {
			anim.SetFloat ("Speed", 0);
			return;
		}
		
	// character is walking
		anim.SetFloat ("Speed", 1);
		currentPathDist += speed * Time.deltaTime * Mathf.Sign (targetDist - currentPathDist);
		//transform.position = path.GetPositionByT(currentPathT);
		
		transform.position = path.GetPositionByDistance(currentPathDist);
		
	}
}
