using UnityEngine;
using System.Collections;

public class Vortex : MonoBehaviour {

	public float radius;
	//public float duration;
	public bool flipDirection;
	public float strength;
	
	void Start() {
		ForceField.Instance.AddVortex(this);
	}
	
	public Vector3 GetForce(Vector3 pos) {
		float actualStrength = GetStrengthAtDistance((pos - transform.position).magnitude);
		if(actualStrength <= .0001f) {
			return Vector3.zero;
		} else {
			
			//The force is a mix of 1) direction towards center, and 2) its perpendicular...
			Vector3 radial = (transform.position - pos).normalized;
			Vector3 circular = Vector3.Cross(radial, Vector3.up).normalized * (flipDirection ? -1f : 1f);
			Vector3 dir = (1f * radial + circular).normalized;
			
			
			return dir.normalized * actualStrength;	
		}
		
	}
	
	public float GetStrengthAtDistance(float dist) {
		return dist > radius ? 0f : Mathf.Pow ((radius - dist) / radius, .5f) * strength;
	}
}
