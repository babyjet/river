using UnityEngine;
using System.Collections.Generic;

public class GUIcoordinator : MonoBehaviour {
	
	public GUIriver guiR;
	
	public void UpdateStats() {
		List<string> ls = TimerCoordinator.Instance.GetStatsForPrint ();
		Debug.Log("updated stats: " + ls.Count + " elements");
		guiR.SetDisplayRows (ls);
	}
	
	#region Singleton
	//Setup singleton:
	private static GUIcoordinator _instance;
	public static GUIcoordinator Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.Log("GUIcoordinator not ready!!");
            }
            return _instance;
        }
    }
	
	void Awake() {
		if(_instance != null) return;
		_instance = this;
	}
	#endregion
}
