using UnityEngine;
using System.Collections.Generic;

public class GUIriver : MonoBehaviour {
	
	public float yStartStats; //Where to start drawing stats
	public float rowHeight;
	
	List<string> statsAsString;
	float width;
	float height;
	
	
	void Start() {
		width = Screen.width;
		height = Screen.height;
	}
	
	void OnGUI() {
		GUI.Label(new Rect(10, 10, 100, 20), "River");
		DrawStats();
	}
	
	public void SetDisplayRows(List<string> toWrite) {
		this.statsAsString = toWrite;
	}
	
	void DrawStats() {
		if(statsAsString == null) return;
		float y = yStartStats;
		
		foreach(string s in statsAsString) {
			GUI.Label(new Rect(10, y, 300, 20), s);
			y += rowHeight;
			//Debug.Log(s);
		}
	}
}
