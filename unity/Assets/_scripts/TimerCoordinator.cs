using UnityEngine;
using System.Collections.Generic;

public class TimerCoordinator : MonoBehaviour {

	List<FloatingObject> timedObjects = new List<FloatingObject>();	
	public List<TimerWaypoint> waypoints = new List<TimerWaypoint>();
	
	public void ReportObject(FloatingObject fo) {
		timedObjects.Add (fo);
	}
	
	public void UpdateStats() {
		
	}
	
	public List<string> GetStatsForPrint() {
		
		//First, a header:
		List<string> output = new List<string>();
		string header = "                    ";
		foreach(FloatingObject o in timedObjects) {
			header += " " + o.name;
		}
		
		output.Add (header);
		
		//Then, the stats:
		foreach(TimerWaypoint w in waypoints) {
			string s = w.name.PadRight(20) + ": ";
			foreach(FloatingObject o in timedObjects) {
				s = s + o.GetWaypointTime(w.name) + "   ";
			}
			output.Add (s);
		}
		
		return output;
		
	}
	
	/*
	public void WriteStatusList() {
		foreach(WaypointEvent e in events) {
			Debug.Log(e.floatingObjectName + " hit waypoint " + e.waypointName + " at time " + e.time);
		}
	}
	*/
	
	#region Singleton
	//Setup singleton:
	private static TimerCoordinator _instance;
	public static TimerCoordinator Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.Log("TimerCoordinator not ready!!");
            }
            return _instance;
        }
    }
	
	void Awake() {
		if(_instance != null) return;
		_instance = this;
	}
	#endregion
	
}
