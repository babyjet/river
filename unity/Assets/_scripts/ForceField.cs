using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class ForceField : MonoBehaviour {
	
	List<Rigidbody> objects = new List<Rigidbody>();
	public GameObject terrain;
	public bool velChangeMode;
	public float zVelocityThreshold;
	public float forceStrength;
	public Vector3 defaultZforce;
	public float maxVelocity;
	public float velocityClamping;
	
	Vector3[,] forceField;
	int nx;
	int nz;
	float dx;
	float dz;
	float startX;
	float startZ;
	bool isForceFieldAvailable = false;
	
	//Splashes:
	Dictionary<int,Splash> splashes = new Dictionary<int,Splash>();
	
	//Vortices:
	List<Vortex> vortices = new List<Vortex>();
	
	
	public static string texURL = "_FieldTex/field.png";
	
	// Use this for initialization
	void Start () {
		
		//Add some starting force:
		/*
		foreach(Rigidbody body in objects) {
			body.AddForce(new Vector3(-20f, 0f, 0f), ForceMode.VelocityChange);
		}
		*/
		
		StartCoroutine(LoadForceField());
		
	}
	
	IEnumerator LoadForceField() {
		WWW www = new WWW("file://" + Application.dataPath +"/" + texURL);
		//Debug.Log("Trying to access texture at " + www.url);
		yield return www;
		forceField = GetForcefieldFromTexture(www.texture);
		
		//Initiate geometry to match mesh:
		nx = forceField.GetLength (0);
		nz = forceField.GetLength (1);
		
		Matrix4x4 mat = terrain.transform.localToWorldMatrix;
		
		startX = (mat * terrain.GetComponent<MeshFilter>().mesh.bounds.min).x + terrain.transform.position.x;
		startZ = (mat * terrain.GetComponent<MeshFilter>().mesh.bounds.min).z + terrain.transform.position.z;
		dx = (mat * terrain.GetComponent<MeshFilter>().mesh.bounds.size).x / (float)nx;
		dz = (mat * terrain.GetComponent<MeshFilter>().mesh.bounds.size).z / (float)nz;
		
		isForceFieldAvailable = true;
	}
	
	void FixedUpdate () {
		if(!isForceFieldAvailable) return;
		List<int> killIndices = new List<int>();
		int i = 0;
		foreach(Rigidbody body in objects) {
			
			if(body.transform.position.z > 0f) {
				killIndices.Add(i);
			}
			
			//body.velocity = .5f*(body.velocity + GetForce(body.transform.position) * 1f);
			Vector3 force = GetForce(body.transform.position);
			
			//Adjust z component so that does not accelerate if at certain speed;
			/*
			if(body.velocity.z <= zVelocityThreshold) {
				force.z = 0f;
			}
			*/
			
			//Debug.Log("Force strength: " + force.magnitude);
			
			//Dampen existing velocity, so fade out x component of speed to align with river:
			
			body.velocity = body.velocity * maxVelocity;
			body.velocity = Vector3.ClampMagnitude(body.velocity, maxVelocity);
			
			body.AddForce(force, velChangeMode ? ForceMode.VelocityChange : ForceMode.Force);
			
			//Fix rotation:
			float rotWeight = .90f;
			body.transform.rotation = Quaternion.Lerp(body.transform.rotation, Quaternion.LookRotation(body.velocity), (1-rotWeight));
			//body.transform.rotation = Quaternion.LookRotation(body.velocity);
			
			
			//Splashes:
			foreach(KeyValuePair<int, Splash> s in splashes) {
				if((s.Value.position - body.transform.position).magnitude < s.Value.radius) {
					Vector3 sf = s.Value.GetForce(body.transform.position);
					sf.y = 0f;
					body.AddForce(sf, velChangeMode ? ForceMode.VelocityChange : ForceMode.Force);
					//Debug.Log("Applying splash force, index: " + s.Key + ", force: " + sf.magnitude);
				}
			}
			
			//Vortices:
			foreach(Vortex v in vortices) {
				Vector3 vf = v.GetForce(body.transform.position);
				vf.y = 0f;
				body.AddForce(vf, velChangeMode ? ForceMode.VelocityChange : ForceMode.Force);
			}
			
			
			
			i++;
		}
		
		foreach(int k in killIndices) {
			Destroy(objects[k].gameObject);
			objects.RemoveAt(k);
		}
	}
	
	public void AddFloatingObject(GameObject go) {
		objects.Add (go.rigidbody);
	}
	
	public void AddVortex(Vortex v) {
		vortices.Add (v);
	}
	
	int splashIndexCycler = 0;
	public void CreateSplash(Vector3 pos, float radius, float duration, float strength) {
		splashIndexCycler = splashIndexCycler + 1 % 10000;
		splashes.Add (splashIndexCycler, new Splash(pos, radius, duration, strength));
		StartCoroutine(RemoveSplash(duration, splashIndexCycler));
	}
	
	IEnumerator RemoveSplash(float delay, int splashIndex) {
		yield return new WaitForSeconds(delay);
		splashes.Remove(splashIndex);
	}
	
	Vector3 GetForce(Vector3 position) {
		
		//Get the indices:
		int x;
		int z;
		
		GetFieldCoordinate(position, out x, out z);
		//Debug.Log("Object at: " + position + ", index: " + x + ", " + z + ". nx,nz = " + nx + "," + nz);
		if(x >= 0 && x < nx && z >= 0 && z < nz) {
			//return (forceField[x, nz - 1 - z] + defaultZforce) * forceStrength;
			return forceField[x, nz - 1 - z].normalized * forceStrength;
		} else {
			return Vector3.zero;
		}
	}
	
	void GetFieldCoordinate(Vector3 pos, out int x, out int z) {
		x = (int)((pos.x - startX) / dx);
		z = (int)((pos.z - startZ) / dz);
	}
	
	int[] GetFieldCoordinate(Vector3 pos) {
		int x = (int)((pos.x - startX) / dx);
		int z = (int)((pos.z - startZ) / dz);
		return new int[]{x, z};
	}
	
	public class Splash {
		public Vector3 position;
		public float radius;
		public float duration;
		public float strength;
		
		public Splash(Vector3 position, float radius, float duration, float strength) {
			this.position = position;
			this.radius = radius;
			this.duration = duration;
			this.strength = strength;
		}
		
		public Vector3 GetForce(Vector3 pos) {
			return (pos - this.position).normalized * GetStrengthAtDistance((pos - this.position).magnitude);
		}
		
		public float GetStrengthAtDistance(float dist) {
			return dist > radius ? 0f : (radius - dist) / radius * strength;
		}
	}

	/*
	Vector3 GetForce(Vector3 position) {
		return -position;
	}
	*/
	
	#region Force field conversion
	public static Vector3[,] GetForcefieldFromTexture(Texture2D tex) {
		int nx = tex.width;
		int nz = tex.height;
		Vector3[,] field = new Vector3[nx, nz];
		for(int i = 0; i < nx; i ++) {
			for(int j = 0; j < nz; j ++) {
				Color c = tex.GetPixel(i,j); 
				field[i,j] = 2f * (new Vector3(c.r, c.g, c.b) - .5f*Vector3.one);
			}
		}
		return field;
	}
	
	public static Texture2D GetTextureFromField(Vector3[,] field) {
		
		int nx = field.GetLength(0);
		int nz = field.GetLength(1);
		
		Texture2D fieldTex = new Texture2D(nx, nz);
		
		for(int i = 0; i < nx; i++) {
			for(int j = 0; j < nz; j++) {
				//Need to ensure value is within 0 - 1
				Vector3 f = field[i,nz - 1 - j]*.5f + .5f*Vector3.one;
				Color c = new Color(f.x, f.y, f.z, 1.0f);
				fieldTex.SetPixel(i, j, c);
			}
		}
		
		return fieldTex;
	}
	#endregion
	
	#region Singleton
	//Setup singleton:
	private static ForceField _instance;
	public static ForceField Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.Log("ForceField not ready!!");
            }
            return _instance;
        }
    }
	
	void Awake() {
		if(_instance != null) return;
		_instance = this;
	}
	#endregion
	
}
