using UnityEngine;
using System.Collections;

public class PathPoint : MonoBehaviour {
	
	public float pathDist;
	public delegate void OnPathPointEvent(PathPoint p);
	public event OnPathPointEvent pathPointClickEvent;
	
	void OnMouseDown() {
		pathPointClickEvent(this);
	}
}
