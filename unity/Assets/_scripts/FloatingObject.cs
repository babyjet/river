using UnityEngine;
using System.Collections.Generic;

public class FloatingObject : MonoBehaviour {

	public string name;
	bool isClockRunning;
	float currentTime;
	Dictionary<string, WaypointEvent> events = new Dictionary<string, WaypointEvent>();
	
	void Start() {
		ForceField.Instance.AddFloatingObject(gameObject);
	}
	
	public void StartClock() {
		//Report our existance to the coordinator:
		TimerCoordinator.Instance.ReportObject(this);
		isClockRunning = true;
	}
	
	public void StopClock() {
		//Debug.Log("Stopping clock on " + name);
		isClockRunning = false;
	}
	
	public float GetCurrentTime() {
		return currentTime;
	}
	
	public void AddWaypointEvent(float time, string waypointName) {
		if(!events.ContainsKey(waypointName)) {
			events.Add (waypointName, new WaypointEvent(time, this.name, waypointName));
		}
	}
	
	public float GetWaypointTime(string waypointKey) {
		if(events.ContainsKey(waypointKey)) {
			return events[waypointKey].time;
		} else {
			return 0f;
		}
	}
	
	void Update() {
		if(isClockRunning) {
			//Debug.Log("time = " + currentTime);
			currentTime += Time.deltaTime;
		}
	}
	
	public void ReportEvent(float time, string waypointName, string objectName) {
		events.Add (waypointName, new WaypointEvent(time, objectName, waypointName));
	}
	

}
