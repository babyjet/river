using UnityEngine;
using System.Collections;

public class WaterClick : MonoBehaviour {

	public ForceField river;
	public Transform player;
	public float playerRange;
	public float splashRadius;
	public float splashStrength;
	public float splashDuration;
	
	void OnMouseDown() {
		Debug.Log("Click!");
		Vector3 mp = Input.mousePosition;
		
        //Vector3 splashPosition = Camera.main.ScreenToWorldPoint(new Vector3(mp.x, mp.y, Camera.main.transform.position.y));
		//Debug.Log("click position world: " + splashPosition);
		
		
    	Ray ray = Camera.main.ScreenPointToRay(mp);
    	RaycastHit hit;
		
        if (Physics.Raycast(ray, out hit)) {
            //Debug.DrawLine(ray.origin, hit.point);
			Vector3 splashPosition = hit.point;
			//Debug.Log("click position world: " + splashPosition);
			if((player.position - splashPosition).magnitude < playerRange) {
				river.CreateSplash(splashPosition, splashRadius, splashDuration, splashStrength);
			} else {
				Debug.Log("Not within range :(");
			}
    	}
		
	}
	
	
	
}
