using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public GameObject followObject1;
	public GameObject followObject2;
	public float cameraHeight;
	float speed = 0f;
	
	public float dist;
	
	void Update () {
		
		dist = (followObject1.transform.position - followObject2.transform.position).magnitude;
		Vector3 newPos  = Vector3.Lerp(transform.position, .5f * (followObject1.transform.position + followObject2.transform.position), .07f);
		newPos.y = Mathf.Max (cameraHeight, dist * 1.7f);
		transform.position = newPos;
		
	}
}
