using UnityEngine;
using System.Collections;

public class ObjectSpawner : MonoBehaviour {
	
	public GameObject objectPrefab;
	public bool doSpawn;
	public float spawnInterval;
	
	
	// Use this for initialization
	void Start () {
		InvokeRepeating("Spawn", 1f, spawnInterval);
	}
	
	int i = 0;
	void Spawn() {
		if(!doSpawn) return;
		float x = Random.Range (-5.7f, -6.6f);
		float z = -.5f;
		float y = 0.02f;
		GameObject go = Instantiate(objectPrefab, new Vector3(x,y,z), Quaternion.identity) as GameObject;
		GetComponent<ForceField>().AddFloatingObject(go);
		i++;
		if(i%20 ==0) {
			go.GetComponent<TrailRenderer>().enabled = true;
		}
	}
}
