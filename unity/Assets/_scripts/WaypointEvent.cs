public class WaypointEvent {
	public float time;
	public string floatingObjectName;
	public string waypointName;
	
	public WaypointEvent (float time, string floatingObjectName, string waypointName)
	{
		this.time = time;
		this.floatingObjectName = floatingObjectName;
		this.waypointName = waypointName;
	}

}