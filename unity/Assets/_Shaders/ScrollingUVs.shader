Shader "River/ScrollingUVs" {
	Properties {
		_Bottom ("Bottom Texture", 2D) = "white" {}
		_Tint ("Tint color", Color) = (1, 1, 1, 1)
		_Refl ("Reflection strength", Range(0,1)) = 0.5
		_RimPower ("Rim Power", Range(0, 5)) = 0.5
		_Cube ("Cubemap", CUBE) = "" { TexGen CubeReflect }
		_Bump ("Normal map", 2D) = "bump" {}
		_Speed1 ("Speed", vector) = (0, 0.05, 0, 0)
		_Speed2 ("Speed2", vector) = (0, -0.03, 0, 0)
		_Offset2 ("Offset2", vector) = (0, 0.5, 0, 0)

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong
		#pragma target 3.0
		//input limit (8) exceeded, shader uses 9
		#pragma exclude_renderers d3d11_9x
		
		sampler2D _Bump;
		sampler2D _Bottom;
		samplerCUBE _Cube;
		fixed4 _Tint;
		float _Refl;
		float4 _Speed1;
		float4 _Speed2;
		float4 _Offset2;
		float _RimPower;

		struct Input {
			float2 uv_Bottom;
			float2 uv_Bump;
            float3 worldRefl;
            float3 viewDir;
			INTERNAL_DATA //Needed for the cubemap to work
			
		};

		void surf (Input IN, inout SurfaceOutput o) {

			o.Normal = normalize(
						UnpackNormal (tex2D (_Bump, IN.uv_Bump + _Time * _Speed1)) +
					   	UnpackNormal (tex2D (_Bump, IN.uv_Bump + _Time * _Speed2 + _Offset2)));
			
			
            half rim = (1-pow(saturate(dot (normalize(IN.viewDir), o.Normal)*2), _RimPower*0.5)) * _Refl;

			
			half4 c = lerp (tex2D (_Bottom, IN.uv_Bottom + (o.Normal*0.1)), 0, rim);
//			c = tex2D (_Bottom, IN.uv_Bottom);
			o.Albedo = c.rgb;
			o.Alpha = c.a;

            o.Emission = lerp (0.2, (texCUBE (_Cube, WorldReflectionVector (IN, o.Normal)).rgb), rim);
			

			
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
