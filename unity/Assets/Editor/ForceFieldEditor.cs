using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

//ToDo: Better source for bounds (now: wall1)
//ToDo: Translation/rotation/scale invariant
//ToDo: Differentiate x and z (resolution)

public class ForceFieldEditor : EditorWindow {

	[MenuItem ("River/Force field generator")]
	public static void ShowForceFieldGenerator() {
		var window = GetWindow <ForceFieldEditor>();
		window.title = "Force Field Generator";
	}
	
	GameObject wall1;
	GameObject wall2;
	float blurRadius;
	int nObst;
	GameObject[] obstacles;
	
	bool useNormalizedStrength = true;
	bool doDrawField = true;
	bool doDrawEdgesAndNormals = true;
	float handleLength;
	float edgeThreshold;
	int resolution;
	Texture2D fieldTex;
	
	//Store calculated results:
	RiverObject riverWall1;
	RiverObject riverWall2;
	RiverObject[] riverObstacles;
	Vector3[,] field;
	bool showObstacleList;	
	
	//Debug:
	GameObject debugObject;
	
	//ToDo: either calculate on all selected, or on all with some tag
	void OnGUI() {
        //EditorGUILayout.BeginHorizontal();
        wall1 = EditorGUILayout.ObjectField("Wall 1", wall1, typeof(GameObject), true, null) as GameObject;
		wall2 = EditorGUILayout.ObjectField("Wall 2", wall2, typeof(GameObject), true, null) as GameObject;	
		
		edgeThreshold = EditorGUILayout.Slider("Edge threshold", edgeThreshold, 0f, 2f, null);
		useNormalizedStrength = EditorGUILayout.Toggle("Normalized strength", useNormalizedStrength);
		
		
		//edgeForce = EditorGUILayout.Slider("Edge force", edgeForce, 1f, 10f, null);
		handleLength = EditorGUILayout.Slider("Handle length", handleLength, 0f, .5f, null);
		resolution = EditorGUILayout.IntSlider("Resolution", resolution, 10, 200, null);
        //EditorGUILayout.EndHorizontal();
		if(GUILayout.Button("Calculate edges") && wall1 != null && wall2 != null) {
			riverWall1 = CreateRiverWall(wall1.GetComponent<MeshFilter>().sharedMesh, wall1.transform.localToWorldMatrix, wall1.transform.position, false);
			riverWall2 = CreateRiverWall(wall2.GetComponent<MeshFilter>().sharedMesh, wall2.transform.localToWorldMatrix, wall2.transform.position, true);
			
			if(obstacles != null && obstacles.Length > 0) {
				riverObstacles = new RiverObject[obstacles.Length];
				for(int i = 0; i < riverObstacles.Length; i ++) {
					riverObstacles[i] = CreateRiverObstacle(obstacles[i].GetComponent<MeshFilter>().sharedMesh, obstacles[i].transform.localToWorldMatrix, obstacles[i].transform.position);
				}
			}
			
			Debug.Log("Attempting to calculate edges...");
		}
		
		if(GUILayout.Button("Calculate field") && wall1 != null && wall2 != null) {
			Debug.Log("Attempting to calculate field...");
			field = CalculateForceField();
			Debug.Log("Attempting to store field in texture...");
			StoreForceField();
		}
		
		//Blur field:
		EditorGUILayout.LabelField(" ");
		blurRadius = EditorGUILayout.Slider("Blur radius", blurRadius, 0f, 2f, null);
		if(GUILayout.Button("Blur field with radius") && wall1 != null && wall2 != null) {
			if(field != null) {
				field = BlurField(blurRadius);
				StoreForceField();
			}
		}
		
		//Obstacles:
		EditorGUILayout.LabelField(" ");
		EditorGUILayout.LabelField("Obstacles:");
		nObst = EditorGUILayout.IntField("Number of obstacles", nObst);
		
		if(GUILayout.Button("Create obstacle list") && nObst > 0) {
			obstacles = new GameObject[nObst];
			
			
			showObstacleList = true;
			//Debug.Log("List: " + obstacles.Count + ", obstacles: " + obstacles);
		}
		
		if(showObstacleList) {
			for(int i = 0; i < obstacles.Length; i ++) {
				obstacles[i] = EditorGUILayout.ObjectField("Obstacle " + i, obstacles[i], typeof(GameObject), true, null) as GameObject;	
			}
		}
		
		//Toggles:
		EditorGUILayout.LabelField(" ");
		EditorGUILayout.LabelField(" ");
		doDrawEdgesAndNormals = EditorGUILayout.Toggle("Draw edges and normals", doDrawEdgesAndNormals);
		doDrawField = EditorGUILayout.Toggle("Draw field", doDrawField);
		
		//Debug:
		EditorGUILayout.LabelField(" ");
		EditorGUILayout.LabelField(" ");
		debugObject = EditorGUILayout.ObjectField("debugObject", debugObject, typeof(GameObject), true, null) as GameObject;	
		if(GUILayout.Button("Refresh debug")) {
			RefreshDebugEdges();
		}
	}
	
	void OnEnable() {
		// Remove delegate listener if it has previously
		// been assigned.
		SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
		 
		// Add (or re-add) the delegate.
		SceneView.onSceneGUIDelegate += this.OnSceneGUI;
	}
	
	void OnDisable() {
		field = null;
	}
		
	void OnSceneGUI(SceneView sv) {
		if(doDrawEdgesAndNormals) DrawEdges();
		if(doDrawEdgesAndNormals) DrawNormals(0.15f);	
		if(doDrawEdgesAndNormals) DrawParallells(0.15f);	
		Handles.color = Color.magenta;
		if(doDrawEdgesAndNormals) DrawVertices(.1f);
	
		if(field != null) {
			if(doDrawField) DrawField();
		}
		
		if(debugObject != null) {
			DrawDebug();
		}
	}
		
	void OnDestroy() {
		// When the window is destroyed, remove the delegate
		// so that it will no longer do any drawing.
		SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
	}
	
	void DrawEdges() {
		Handles.color = Color.red;
		if(riverWall1 != null) riverWall1.DrawEdges();
		if(riverWall2 != null) riverWall2.DrawEdges();
		
		if(riverObstacles != null) {
			foreach(RiverObject ro in riverObstacles) {
				if(ro != null) ro.DrawEdges();
			}
		}
	}
	
	void DrawParallells(float size) {
		Handles.color = Color.red;
		if(riverWall1 != null) riverWall1.DrawParallells(size);
		if(riverWall2 != null) riverWall2.DrawParallells(size);
		
		if(riverObstacles != null) {
			foreach(RiverObject ro in riverObstacles) {
				if(ro != null) ro.DrawParallells(size);
			}
		}
	}
	
	Vector3 GetNormal(Vector3 v1, Vector3 v2) {
		return Vector3.Cross(v2 - v1, new Vector3(0f, 1f, 0f)).normalized;
	}
	
	void DrawNormals(float size) {
		Handles.color = Color.green;
		if(riverWall1 != null) riverWall1.DrawNormals(size);
		if(riverWall2 != null) riverWall2.DrawNormals(size);
		
		if(riverObstacles != null) {
			foreach(RiverObject ro in riverObstacles) {
				if(ro != null) ro.DrawNormals(size);
			}
		}
	}
	
	void DrawVertices(float heightAboveFlat) {
		if(riverWall1 != null) riverWall1.DrawVertices(heightAboveFlat);
		if(riverWall2 != null) riverWall2.DrawVertices(heightAboveFlat);
	}

	void DrawField() {
		//ToDo: use some general object for the matrix and bounds
		Matrix4x4 mat = wall1.transform.localToWorldMatrix;
		float startX = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.min).x + wall1.transform.position.x;
		float startZ = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.min).z + wall1.transform.position.z;
		float dx = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.size).x / (float)resolution;
		float dz = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.size).z / (float)resolution;
		Handles.color = Color.green;
		for(int i = 0; i < resolution; i++) {
			for(int j = 0; j < resolution; j++) {
				float x = startX + .5f * dx + i * dx;
				float z = startZ + .5f * dz + j * dz;
				Vector3 p1 = new Vector3(x, 0f, z);
				Vector3 p2 = p1 + field[i,j] * handleLength;
				Handles.DrawLine(p1, p2);
				
			}
		}
	}
		
	RiverObject CreateRiverWall(Mesh mesh, Matrix4x4 mat, Vector3 anchorPosition, bool flipEntireObject) {
		List<EdgeVector> edgeVectors = new List<EdgeVector>();
		Vector3[] verts = mesh.vertices;
		foreach(Edge e in BuildManifoldEdges(mesh)) {
			Vector3 v1 = mat * verts[e.vertexIndex[0]];
			Vector3 v2 = mat * verts[e.vertexIndex[1]];
			edgeVectors.Add (new EdgeVector(v1 + anchorPosition, v2 + anchorPosition, flipEntireObject));
		}
		return new RiverObject(edgeVectors, mesh);
	}
	
	RiverObject CreateRiverObstacle(Mesh mesh, Matrix4x4 mat, Vector3 anchorPosition) {
		List<EdgeVector> edgeVectors = new List<EdgeVector>();
		Vector3[] verts = mesh.vertices;
		foreach(Edge e in BuildManifoldEdges(mesh)) {
			Vector3 v1 = mat * verts[e.vertexIndex[0]];
			Vector3 v2 = mat * verts[e.vertexIndex[1]];
			
			//ToDo: Determine bool flip for this edge, by finding the closes river wall edge and choosing the direction that has a positive dot product with that edge
			EdgeVector evTmp = new EdgeVector(v1 + anchorPosition, v2 + anchorPosition, false);
			EdgeVector evWall = riverWall1.GetClosestEdge(evTmp.GetMidpoint());
			bool doFlip = Vector3.Dot(evTmp.GetParallell(false), evWall.GetParallell()) < 0;
			
			edgeVectors.Add (new EdgeVector(v1 + anchorPosition, v2 + anchorPosition, doFlip));
		}
		return new RiverObject(edgeVectors, mesh);
	}
	
	EdgeVector debugEdge1;
	EdgeVector debugEdge2;
	void RefreshDebugEdges() {
		if(debugObject == null) return;
		EdgeVector[] evClosest = Get2ClosestEdges(debugObject.transform.position);
		debugEdge1 = evClosest[0];
		debugEdge2 = evClosest[1];
	}
	
	void DrawDebug() {
		if(debugObject == null || debugEdge1 == null || debugEdge2 == null) {
			return;
		}
		
		Handles.color = Color.blue;
		debugEdge1.DrawEdge();
		debugEdge2.DrawEdge();
		Handles.Label(debugEdge1.GetMidpoint(), "" + debugEdge1.GetDistanceToEdge(debugObject.transform.position));
		Handles.Label(debugEdge2.GetMidpoint(), "" + debugEdge2.GetDistanceToEdge(debugObject.transform.position));
	}
		
	Vector3[,] CalculateForceField() {
		
		//Algo: 
		//1. For each point, check which edges from 2 different shapes are the closest to the point (distance to midpoint?).
		//2. The direction in that point is given by a * e1 + (1-a) * e2, where a in (0,1) is the relative distance between the midpoints of the edges (simplification!!)
		//   and e1 and e2 are given by r²p1 + (1-r²)n1, where r in (0, 1) is the relative distance from the edge to some threshold distance (after which n1 (the normal) has no infulence)
		//   Innovation: Weighting by distance to midpoint can be bad far from the midpoint. Replace with [distance to edge]
		//3. In 2, n1 is the edge normal, and p1 is the "normal-normal", i.e. parallell to the edge. To find the direction (2 possible, duh), pick the one that has a positive dot product 
		//   with the closest (by midpoint) river wall edge.
		
		//ToDo: Use some general object for matrix and bounds (as in draw field)
		Matrix4x4 mat = wall1.transform.localToWorldMatrix;
		float startX = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.min).x + wall1.transform.position.x;
		float startZ = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.min).z + wall1.transform.position.z;
		float dx = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.size).x / (float)resolution;
		float dz = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.size).z / (float)resolution;
		Vector3[,] field = new Vector3[resolution,resolution];
		
		//Calculate walls:
		riverWall1 = CreateRiverWall(wall1.GetComponent<MeshFilter>().sharedMesh, wall1.transform.localToWorldMatrix, wall1.transform.position, false);
		riverWall2 = CreateRiverWall(wall2.GetComponent<MeshFilter>().sharedMesh, wall2.transform.localToWorldMatrix, wall2.transform.position, true);
		
		for(int i = 0; i < resolution; i++) {
			for(int j = 0; j < resolution; j++) {
				
				float x = startX + .5f * dx + i * dx;
				float z = startZ + .5f * dz + j * dz;
				Vector2 p = new Vector2(x, z);
				Vector3 p3 = new Vector3(x, 0f, z);
				
				//Check if point is on land:
				bool isOnLand = riverWall1.ContainsPoint(p3) || riverWall2.ContainsPoint(p3);
				//Obstacles:
				if(!isOnLand && riverObstacles != null) {
					foreach(RiverObject ro in riverObstacles) {
						isOnLand = ro.ContainsPoint(p3);
						if(isOnLand) break;
					}
				}
				
				if(isOnLand) {
					field[i,j] = Vector3.zero;
					//Debug.Log("Point on land: " + p3);
				} else {
				
					//Get closest edge from wall 1 and wall 2:
					//EdgeVector ev1 = riverWall1.GetClosestEdge(p3);
					//EdgeVector ev2 = riverWall2.GetClosestEdge(p3);
					
					EdgeVector[] evClosest = Get2ClosestEdges(p3);
					EdgeVector ev1 = evClosest[0];
					EdgeVector ev2 = evClosest[1];
					
					//If closer to edge vertex than to midpoint, then weigh in vertex direction
					float vDist1 = Mathf.Min ((p3 - ev1.start).magnitude, (p3 - ev1.end).magnitude); 
					float vDist2 = Mathf.Min ((p3 - ev2.start).magnitude, (p3 - ev2.end).magnitude); 
					float mpDist1 = (p3 - ev1.GetMidpoint()).magnitude;
					float mpDist2 = (p3 - ev2.GetMidpoint()).magnitude;
					
					Vector3 direction1;
					Vector3 direction2;
					
					if(vDist1 < mpDist1  /*&& i >= 8 && i <= resolution - 8 && j >= 8 && j <= resolution - 8*/) {
						//float a = vDist1 / (0.25f * (ev1.start - ev1.end).magnitude); //We know the largest possible value for vDist1 is 1/4 of the edge length!
						//direction1 = a * ev1.GetParallell() + (1f - a) * riverWall1.GetClosestEdgeVertexParellell(p3);
						direction1 = riverWall1.GetClosestEdgeVertexParellell(p3);
					} else {
						direction1 = ev1.GetParallell(); //If far from vertex, then take only the "pure" parallell
					}
					
					if(vDist2 < mpDist2  /*&& i >= 8 && i <= resolution - 8 && j >= 8 && j <= resolution - 8*/) {
						//float a = vDist2 / (0.25f * (ev2.start - ev2.end).magnitude); //We know the largest possible value for vDist1 is 1/4 of the edge length!
						//direction2 = a * ev2.GetParallell() + (1f - a) * riverWall2.GetClosestEdgeVertexParellell(p3);
						direction2 = riverWall2.GetClosestEdgeVertexParellell(p3);
					} else {
						direction2 = ev2.GetParallell();
					}
					
	
					float d1 = ev1.GetDistanceToEdge(p3);
					float d2 = ev2.GetDistanceToEdge(p3);
					
					//Force from edge, inward:
					if(d1 < edgeThreshold) {
						direction1 = (direction1 + ev1.GetNormal() * 3f).normalized;
					} 
					
					if(d2 < edgeThreshold) {
						direction2 = (direction2 + ev2.GetNormal() * 3f).normalized;
					} 
					
					//Use the weighted direction:
					Vector3 v = (d1 * direction2 + d2 * direction1).normalized;
					
					//ToDo: force proportional to distance between edges!
					if(!useNormalizedStrength) {
						v = v / (d1 + d2);
					}
					
					//Weight one vector with the distance to the opposite wall and vice versa
					//Vector3 v = (d1 * ev2.GetParallell() + d2 * ev1.GetParallell()).normalized;
					field[i,j] = v;
				}
				
				
			}
		}
		return field;
	}
	
	EdgeVector[] Get2ClosestEdges(Vector3 pos) {
		
		int nObstacles = obstacles == null ? 0 : obstacles.Length;
		
		EdgeVector[] allEdges = new EdgeVector[nObstacles + 2]; //2 for the walls
		float[] dists = new float[nObstacles + 2];
		
		//First, closest wall edges:
		allEdges[0] = riverWall1.GetClosestEdge(pos, out dists[0]);
		allEdges[1] = riverWall2.GetClosestEdge(pos, out dists[1]);
		
		if(riverObstacles == null) {
			return new EdgeVector[2]{allEdges[0], allEdges[1]}; 
		}
		//Obstacles:
		for(int i = 0; i < riverObstacles.Length; i ++) {
			allEdges[i + 2] = riverObstacles[i].GetClosestEdge(pos, out dists[i + 2]);
		}
		
		//Sort:
		System.Array.Sort(dists, allEdges);
		return new EdgeVector[2]{allEdges[0], allEdges[1]};
	}
	
	Vector3[,] BlurField(float radius) {
		Matrix4x4 mat = wall1.transform.localToWorldMatrix;
		Vector3[,] blurredField = new Vector3[field.GetLength(0),field.GetLength(1)];
		float dx = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.size).x / (float)resolution;
		float dz = (mat * wall1.GetComponent<MeshFilter>().sharedMesh.bounds.size).z / (float)resolution;
		
		int nx = (int)(radius / dx);
		int nz = (int)(radius / dz);
		
		Debug.Log ("blurring with nx,nz = " + nx + ", " + nz + ", radius: " + radius + ", dx: " + dx + ", x size: " + (wall1.GetComponent<MeshFilter>().sharedMesh.bounds.size).x);
		
		float max = 0f; //Use for post-normalization
		
		for(int i = 0; i < resolution; i ++) {
			for(int j = 0; j < resolution; j ++) {
				
				Vector3 tmp = Vector3.zero;
				//Loop through "blur square":
				for(int x = i - nx; x <= i + nx; x ++) {
					for(int z = j - nz; z <= j + nz; z ++) {
						int xx = MinMax(x, 0, resolution - 1);
						int zz = MinMax(z, 0, resolution - 1);
						
						//Weighting like this will create a bias for vectors with magnitude, since some are close to edges... (weighting in zeros)
						tmp += field[xx, zz];// * ((float)(nx - Mathf.Abs (x - i)) + (float)(nz - Mathf.Abs (z - j)));
					}
				}
				
				if(field[i,j].magnitude < .001f) {
					blurredField[i,j] = Vector3.zero;
				} else {
					blurredField[i,j] = tmp;
					//blurredField[i,j] = tmp.normalized * Logistic(tmp.magnitude / 20f);//.normalized; //Issue here... if normalized, all width dependent magnitudes are lost :/
					max = Mathf.Max (max, tmp.magnitude);
				}
				
			}
		}
			
		//Normalize based on max value
		for(int i = 0; i < resolution; i ++) {
			for(int j = 0; j < resolution; j ++) {
				blurredField[i,j] = blurredField[i,j].normalized * blurredField[i,j].magnitude / max;
			}
		}
		
		return blurredField;
	}
	
	//Maps (0, Inf) to (0,1)
	float Logistic(float x) {
		return ((1f / (1 + Mathf.Exp (-x))) - .5f) * 2f;
	}
	
	static int MinMax(int val, int min, int max) {
		return Mathf.Max (Mathf.Min (val, max), min);
	}
	
	#region Special classes
	//Represents a shape in the river, either a wall or an obstacle
	public class RiverObject {
		List<EdgeVector> edgeVectors;
		Mesh mesh;
		
		public RiverObject(List<EdgeVector> edgeVectors, Mesh mesh) {
			this.edgeVectors = edgeVectors;
			this.mesh = mesh;
		}
		
		public bool ContainsPoint(Vector3 point) {
			Vector2[] verts = GetOrderedVertexLoop();
			
			bool hej = ForceFieldEditor.ContainsPoint(verts, new Vector2(point.x, point.z));
			//Debug.Log("Checking point " + point + ", ev: " + edgeVectors[0].start + ", contains?" + hej);
			return hej;
		}
		
		public Vector2[] GetOrderedVertexLoop() {
			Vector2[] verts = new Vector2[edgeVectors.Count];
			int currentEdgeIndex = 0;
			//Vector3 startVertex = edgeVectors[currentEdgeIndex].start;
			//verts[0] = startVertex;
			for(int i = currentEdgeIndex; i < verts.Length; i ++) {
							
				int tmp;
				Vector3 v = GetClosestStartVertex(edgeVectors[currentEdgeIndex].end, out tmp);
				currentEdgeIndex = tmp;
				verts[i] = new Vector2(v.x, v.z);
			}
			return verts;
		}
		
		public EdgeVector GetClosestEdge(Vector3 pos) {
			float foo;
			return GetClosestEdge(pos, out foo);
		}
		
		public EdgeVector GetClosestEdge(Vector3 pos, out float distance) {
			float minDistance = 999999f;
			int minDistanceIndex = 0;
			int i = 0;
			foreach(EdgeVector ev in edgeVectors) {
				float dist = ev.GetDistanceToMidpoint(pos);
				if(dist < minDistance) {
					minDistanceIndex = i;
					minDistance = dist;
				}
				i++;
			}
			distance = minDistance;
			return edgeVectors[minDistanceIndex];
		}
		
		public Vector3 GetClosestEdgeVertexParellell(Vector3 pos) {
			
			//Find the ordered vertices:
			Vector2[] verts = GetOrderedVertexLoop();
			
			//Find the vertex closest to our point:
			Vector2 p2 = new Vector2(pos.x, pos.z);
			float minDistance = 999999f;
			int minDistanceIndex = 0;
			int i = 0;
			foreach(Vector2 v in verts) {
				float dist = (v - p2).magnitude;
				if(dist < minDistance) {
					minDistanceIndex = i;
					minDistance = dist;
				}
				i++;
			}
			
			//Find start, mid and end in order to calculate the 2 normals:
			Vector3 v1 = GetVector3(verts[(minDistanceIndex - 1 + verts.Length) % verts.Length]);
			Vector3 v2 = GetVector3(verts[minDistanceIndex]);
			Vector3 v3 = GetVector3(verts[(minDistanceIndex + 1) % verts.Length]);
			
			Vector3 parallell1 = GetClosestEdge(.5f*(v1 + v2)).GetParallell();
			Vector3 parallell2 = GetClosestEdge(.5f*(v2 + v3)).GetParallell();
			
			return (parallell1 + parallell2).normalized;
			
		}
				
		static Vector3 GetVector3(Vector2 v2) {
			return new Vector3(v2.x, 0f, v2.y);
		}
		
		public void DrawEdges() {
			foreach(EdgeVector ev in edgeVectors) {
				ev.DrawEdge();
			}
		}
		
		public void DrawParallells(float size) {
			foreach(EdgeVector ev in edgeVectors) {
				ev.DrawParallell(size);
			}
		}
		
		public void DrawNormals(float size) {
			foreach(EdgeVector ev in edgeVectors) {
				ev.DrawNormal(size);
			}
		}
		
		public void DrawVertices(float heightAboveFlat) {
			int i = 0;
			foreach(Vector2 v in GetOrderedVertexLoop()) {
				Handles.Label(new Vector3(v.x, heightAboveFlat, v.y), "" + i);
				i++;
			}
		}
		
		public Vector3 GetClosestStartVertex(Vector3 pos, out int edgeIndex) {
			float minDistance = 999999f;
			int minDistanceIndex = 0;
			int i = 0;
			foreach(EdgeVector ev in edgeVectors) {
				float dist = (pos - ev.start).magnitude;
				if(dist < minDistance) {
					minDistanceIndex = i;
					minDistance = dist;
				}
				i++;
			}
			edgeIndex = minDistanceIndex;
			return edgeVectors[minDistanceIndex].start;
		}
	}
	
	//Represents an edge inside a RiverObject.
	public class EdgeVector {
		public Vector3 start;
		public Vector3 end;
		public bool flip;
		
		public EdgeVector (Vector3 start, Vector3 end, bool flip) {
			this.start = start;
			this.end = end;
			this.flip = flip;
		}
		
		public Vector3 GetNormal() {
			return Vector3.Cross ((end - start), new Vector3(0f, 1f, 0f)).normalized;
		}
		
		public Vector3 GetParallell() {
			return GetParallell(this.flip);
		}
		
		public Vector3 GetParallell(bool _flip) {
			return _flip ? (start - end).normalized : (end - start).normalized;
		}
		
		public Vector3 GetMidpoint() {
			return .5f*(start + end);
		}
		
		public float GetDistanceToMidpoint(Vector3 pos) {
			return (pos - GetMidpoint()).magnitude;
		}
		
		public float GetDistanceToEdge(Vector3 pos) {
			return Vector3.Cross((end - start).normalized, pos - start).magnitude;
		}
		
		public void DrawEdge() {
			Handles.DrawLine(start, end);
		}
		
		public void DrawNormal(float size) {
			Vector3 mid = GetMidpoint();
			Handles.DrawLine(mid, mid + GetNormal() * size);
		}
		
		public void DrawParallell(float size) {
			Vector3 n = GetMidpoint() + size * GetNormal();
			Handles.ArrowCap(0, n, Quaternion.LookRotation(GetParallell()), size);
		}
			
		public void DrawStartVertex(string label) {
			Handles.Label(start, label);
		}

	}
	#endregion

	#region IO
	void StoreForceField() {
		SaveTextureToFile(ForceField.GetTextureFromField(field), ForceField.texURL);
	}
	
    void SaveTextureToFile(Texture2D tex,string fileName) {
		FileStream fs = new FileStream(Application.dataPath +"/" + fileName, FileMode.Create);
		BinaryWriter bw = new BinaryWriter(fs);
		bw.Write(tex.EncodeToPNG());
		bw.Close();
		fs.Close();
		Debug.Log("end of save");
    }
	#endregion	
	
	#region Helpers...
	Vector2 GetXZ(Vector3 v) {
		return new Vector2(v.x, v.z);				
	}
	
	static bool ContainsPointYFlat(Vector3[] polyPoints, Vector2 p) {
		Vector2[] polyPoints2D = new Vector2[polyPoints.Length];
		int i = 0;
		foreach(Vector3 v3 in polyPoints) {
			polyPoints[i] = new Vector2(v3.x, v3.z);
			i++;
		}
		return ContainsPoint(polyPoints2D, p);
	}
	
	static bool ContainsPoint (Vector2[] polyPoints, Vector2 p) { 
		int j = polyPoints.Length - 1; 
		bool inside = false; 
		for (int i = 0; i < polyPoints.Length; j = i++) { 
			if ( ((polyPoints[i].y <= p.y && p.y < polyPoints[j].y) || (polyPoints[j].y <= p.y && p.y < polyPoints[i].y)) && 
			(p.x < (polyPoints[j].x - polyPoints[i].x) * (p.y - polyPoints[i].y) / (polyPoints[j].y - polyPoints[i].y) + polyPoints[i].x)) 
				inside = !inside; 
			} 
		return inside; 
	}
	#endregion
		
	#region polygon logic
	public class Edge
	{
		// The indiex to each vertex
		public int[]  vertexIndex = new int[2];
		// The index into the face.
		// (faceindex[0] == faceindex[1] means the edge connects to only one triangle)
		public int[]  faceIndex = new int[2];
	}
	
	/// Builds an array of edges that connect to only one triangle.
	/// In other words, the outline of the mesh	
	public static Edge[] BuildManifoldEdges (Mesh mesh)
	{
		// Build a edge list for all unique edges in the mesh
		Edge[] edges = BuildEdges(mesh.vertexCount, mesh.triangles);
		
		// We only want edges that connect to a single triangle
		ArrayList culledEdges = new ArrayList();
		foreach (Edge edge in edges)
		{
			if (edge.faceIndex[0] == edge.faceIndex[1])
			{
				culledEdges.Add(edge);
			}
		}

		return culledEdges.ToArray(typeof(Edge)) as Edge[];
	}

	/// Builds an array of unique edges
	/// This requires that your mesh has all vertices welded. However on import, Unity has to split
	/// vertices at uv seams and normal seams. Thus for a mesh with seams in your mesh you
	/// will get two edges adjoining one triangle.
	/// Often this is not a problem but you can fix it by welding vertices 
	/// and passing in the triangle array of the welded vertices.
	public static Edge[] BuildEdges(int vertexCount, int[] triangleArray)
	{
		int maxEdgeCount = triangleArray.Length;
		int[] firstEdge = new int[vertexCount + maxEdgeCount];
		int nextEdge = vertexCount;
		int triangleCount = triangleArray.Length / 3;
		
		for (int a = 0; a < vertexCount; a++)
			firstEdge[a] = -1;
			
		// First pass over all triangles. This finds all the edges satisfying the
		// condition that the first vertex index is less than the second vertex index
		// when the direction from the first vertex to the second vertex represents
		// a counterclockwise winding around the triangle to which the edge belongs.
		// For each edge found, the edge index is stored in a linked list of edges
		// belonging to the lower-numbered vertex index i. This allows us to quickly
		// find an edge in the second pass whose higher-numbered vertex index is i.
		Edge[] edgeArray = new Edge[maxEdgeCount];
		
		int edgeCount = 0;
		for (int a = 0; a < triangleCount; a++)
		{
			int i1 = triangleArray[a*3 + 2];
			for (int b = 0; b < 3; b++)
			{
				int i2 = triangleArray[a*3 + b];
				if (i1 < i2)
				{
					Edge newEdge = new Edge();
					newEdge.vertexIndex[0] = i1;
					newEdge.vertexIndex[1] = i2;
					newEdge.faceIndex[0] = a;
					newEdge.faceIndex[1] = a;
					edgeArray[edgeCount] = newEdge;
					
					int edgeIndex = firstEdge[i1];
					if (edgeIndex == -1)
					{
						firstEdge[i1] = edgeCount;
					}
					else
					{
						while (true)
						{
							int index = firstEdge[nextEdge + edgeIndex];
							if (index == -1)
							{
								firstEdge[nextEdge + edgeIndex] = edgeCount;
								break;
							}
						
							edgeIndex = index;
						}
					}
			
					firstEdge[nextEdge + edgeCount] = -1;
					edgeCount++;
				}
			
				i1 = i2;
			}
		}
		
		// Second pass over all triangles. This finds all the edges satisfying the
		// condition that the first vertex index is greater than the second vertex index
		// when the direction from the first vertex to the second vertex represents
		// a counterclockwise winding around the triangle to which the edge belongs.
		// For each of these edges, the same edge should have already been found in
		// the first pass for a different triangle. Of course we might have edges with only one triangle
		// in that case we just add the edge here
		// So we search the list of edges
		// for the higher-numbered vertex index for the matching edge and fill in the
		// second triangle index. The maximum number of comparisons in this search for
		// any vertex is the number of edges having that vertex as an endpoint.
		
		for (int a = 0; a < triangleCount; a++)
		{
			int i1 = triangleArray[a*3+2];
			for (int b = 0; b < 3; b++)
			{
				int i2 = triangleArray[a*3+b];
				if (i1 > i2)
				{
					bool foundEdge = false;
					for (int edgeIndex = firstEdge[i2]; edgeIndex != -1;edgeIndex = firstEdge[nextEdge + edgeIndex])
					{
						Edge edge = edgeArray[edgeIndex];
						if ((edge.vertexIndex[1] == i1) && (edge.faceIndex[0] == edge.faceIndex[1]))
						{
							edgeArray[edgeIndex].faceIndex[1] = a;
							foundEdge = true;
							break;
						}
					}
					
					if (!foundEdge)
					{
						Edge newEdge = new Edge();
						newEdge.vertexIndex[0] = i1;
						newEdge.vertexIndex[1] = i2;
						newEdge.faceIndex[0] = a;
						newEdge.faceIndex[1] = a;
						edgeArray[edgeCount] = newEdge;
						edgeCount++;
					}
				}
				
				i1 = i2;
			}
		}
		
		Edge[] compactedEdges = new Edge[edgeCount];
		for (int e=0;e<edgeCount;e++)
			compactedEdges[e] = edgeArray[e];
		
		return compactedEdges;
	}
	#endregion
	
	/*-----------------------------------*/
		
	#region Old Code
	/*
	void DrawRects() {		
		foreach(Vector3[] rect in GetRects()) {
			Handles.DrawSolidRectangleWithOutline(rect, new Color(1f, 1f, 1f, .2f), Color.red);
		}
	}
	List<Vector3[]> GetRects() {
		List<Vector3[]> rects = new List<Vector3[]>();
		Vector3[] verts = calcMesh.GetComponent<MeshFilter>().sharedMesh.vertices;
		Handles.color = Color.yellow;
		foreach(Edge e in manifoldEdges) {
			Vector3 v1 = mat * verts[e.vertexIndex[0]];
			Vector3 v2 = mat * verts[e.vertexIndex[1]];
			
			Vector3 n = GetNormal(v1, v2);
			
			Vector3 v3 = v2 + n * rectHeight;
			Vector3 v4 = v1 + n * rectHeight;
			
			rects.Add(new Vector3[]{v1, v2, v3, v4});
		}
		return rects;
	}
	
	List<Vector3[]> GetRectsWithNormals() {
		List<Vector3[]> rectsWithNormals = new List<Vector3[]>();
		Vector3[] verts = calcMesh.GetComponent<MeshFilter>().sharedMesh.vertices;
		Handles.color = Color.yellow;
		foreach(Edge e in manifoldEdges) {
			Vector3 v1 = mat * verts[e.vertexIndex[0]];
			Vector3 v2 = mat * verts[e.vertexIndex[1]];
			
			Vector3 n = GetNormal(v1, v2);
			
			Vector3 v3 = v2 + n * rectHeight;
			Vector3 v4 = v1 + n * rectHeight;
			
			rectsWithNormals.Add(new Vector3[]{v1, v2, v3, v4, n});
		}
		return rectsWithNormals;
	}
	
	Vector3[,] CalculateForceField() {
		
		float startX = (mat * calcMesh.GetComponent<MeshFilter>().sharedMesh.bounds.min).x + calcMesh.transform.position.x;
		float startZ = (mat * calcMesh.GetComponent<MeshFilter>().sharedMesh.bounds.min).z + calcMesh.transform.position.z;
		float dx = (mat * calcMesh.GetComponent<MeshFilter>().sharedMesh.bounds.size).x / (float)resolution;
		float dz = (mat * calcMesh.GetComponent<MeshFilter>().sharedMesh.bounds.size).z / (float)resolution;
		Vector3[,] field = new Vector3[resolution,resolution];
		
		//Check edge rects:
		List<Vector3[]> rects = GetRectsWithNormals();
		
		Debug.Log("Start x = " + startX + ", dx = " + dx + ", n = " + resolution + ", rects: " + rects.Count);
		
		Vector3 riverForceVector = new Vector3(0f, 0f, -riverForce);
		
		for(int i = 0; i < resolution; i++) {
			for(int j = 0; j < resolution; j++) {
				float x = startX + .5f * dx + i * dx;
				float z = startZ + .5f * dz + j * dz;
				Vector2 p = new Vector2(x, z);
				
				float rCount = 0f;
				Vector3 tmp = Vector3.zero;
				foreach(Vector3[] rect in rects) {
					//Is iterator within this rect?
					Vector2[] r = new Vector2[]{GetXZ(rect[0]), GetXZ(rect[1]), GetXZ(rect[2]), GetXZ(rect[3])};
					if(ContainsPoint(r, p)) {
						
						//Find the distance to the edge: from mid edge to point
						Vector3 p3 = new Vector3(p.x, 0f, p.y);
						float distance = HandleUtility.DistancePointLine(p3, rect[0], rect[1]);

						//Squared for behaviour!!!
						tmp += rect[4] * (rectHeight - distance) / rectHeight * (rectHeight - distance) / rectHeight * edgeForce + riverForceVector;

					} else {
						tmp += riverForceVector;
					}
					rCount++;
				}
				
				//Finally, store value in matrix:
				if(rCount >= 1f) {
					field[i,j] = (1f/rCount) * tmp;
				}
				
			}
		}
		return field;
	}
	*/
	
	#endregion
	
}

